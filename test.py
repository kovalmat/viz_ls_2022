import sys, random, math
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')  # Use Qt5Agg backend
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLabel
from PySide6.QtCore import Qt, QSize
from PySide6.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QGraphicsView, QSizePolicy
from PySide6.QtGui import QBrush, QPen, QTransform, QPainter
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint, Callback


train_acc = []
val_acc = []
predicted_labels = []
class VisGraphicsScene(QGraphicsScene):
    def __init__(self):
        super(VisGraphicsScene, self).__init__()
        self.selection = None
        self.wasDragg = False
        self.pen = QPen(Qt.black)
        self.selected = QPen(Qt.red)

    def mouseReleaseEvent(self, event):
        if (self.wasDragg):
            return
        if (self.selection):
            self.selection.setPen(self.pen)
        item = self.itemAt(event.scenePos(), QTransform())
        if (item):
            item.setPen(self.selected)
            self.selection = item


class VisGraphicsView(QGraphicsView):
    def __init__(self, scene, parent):
        super(VisGraphicsView, self).__init__(scene, parent)
        self.startX = 0.0
        self.startY = 0.0
        self.distance = 0.0
        self.myScene = scene
        self.setRenderHints(QPainter.Antialiasing | QPainter.TextAntialiasing | QPainter.SmoothPixmapTransform)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorViewCenter)

    def wheelEvent(self, event):
        zoom = 1 + event.angleDelta().y() * 0.001;
        self.scale(zoom, zoom)

    def mousePressEvent(self, event):
        self.startX = event.pos().x()
        self.startY = event.pos().y()
        self.myScene.wasDragg = False
        super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        endX = event.pos().x()
        endY = event.pos().y()
        deltaX = endX - self.startX
        deltaY = endY - self.startY
        distance = math.sqrt(deltaX * deltaX + deltaY * deltaY)
        if (distance > 5):
            self.myScene.wasDragg = True
        super().mouseReleaseEvent(event)


class MainWindow(QMainWindow):
    def __init__(self, train_acc=None, val_acc=None):
        super().__init__()
        self.setWindowTitle("Training and validation accuracy")

        self.createGraphicView()

        # Create a Matplotlib figure and canvas
        self.figure = Figure(figsize=(5, 5), dpi=100)
        self.canvas = FigureCanvas(self.figure)

        # Create the Matplotlib axes
        self.axes = self.figure.add_subplot(111)

        # Plot the training and validation accuracy values for each epoch
        epochs = range(1, len(train_acc) + 1)
        self.axes.plot(epochs, train_acc, 'bo', label='Training accuracy')
        self.axes.plot(epochs, val_acc, 'b', label='Validation accuracy')
        self.axes.set_title('Training and validation accuracy')
        self.axes.set_xlabel('Epochs')
        self.axes.set_ylabel('Accuracy')
        self.axes.legend()

        # Create a layout and add the Matplotlib canvas
        layout = QVBoxLayout()
        layout.addWidget(self.canvas)

        # Create a central widget and set the layout
        central_widget = QWidget()
        central_widget.setLayout(layout)

        # Set the central widget of the main window
        self.setCentralWidget(central_widget)

    def createGraphicView(self):
        self.scene = VisGraphicsScene()
        self.brush = [QBrush(Qt.yellow), QBrush(Qt.green), QBrush(Qt.blue)]
        self.view = VisGraphicsView(self.scene, self)
        self.setCentralWidget(self.view)
        self.view.setGeometry(0, 0, 800, 600)

class SavePredictionCheckpoint(Callback):
    def __init__(self, model, X_test):
        super(SavePredictionCheckpoint, self).__init__()
        self.model = model
        self.X_test = X_test
        self.predicted_labels = []
    def on_epoch_end(self, epoch, logs=None):
        # Predict labels for the input data
        predicted_labels = self.model.predict(self.X_test)  # Assuming X_train contains the input images

        # Append the predicted labels to the list
        self.predicted_labels.append(predicted_labels)
def train():
    # Load the MNIST dataset
    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    # Reshape the input data and normalize it
    X_train = X_train.reshape(60000, 784).astype('float32') / 255
    X_test = X_test.reshape(10000, 784).astype('float32') / 255

    # Convert the labels to one-hot encoded vectors
    nb_classes = 10
    Y_train = to_categorical(y_train, nb_classes)
    Y_test = to_categorical(y_test, nb_classes)

    # Define the model architecture
    model = Sequential()
    model.add(Dense(512, input_shape=(784,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(10))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Checkpoint during learning

    checkpoint = SavePredictionCheckpoint(model, X_test)

    # Train the model and collect the training and validation accuracy values for each epoch
    history = model.fit(X_train, Y_train, batch_size=128, epochs=10, verbose=1, validation_data=(X_test, Y_test), callbacks=[checkpoint])
    train_acc.extend(history.history['accuracy'])
    val_acc.extend(history.history['val_accuracy'])

    predicted_labels = checkpoint.predicted_labels
    #Kolik cisel v datasetu
    digit_counts = np.bincount(y_test)

    # WRITE OUTPUT
    with open("output.txt", 'w') as file:
        file.write("Train Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in train_acc))
        file.write('\n\n')

        # Write val_acc to the file
        file.write("Validation Accuracy:\n")
        file.write('\n'.join(str(acc) for acc in val_acc))
        file.write('\n\n')

        file.write("Predicted Labels:\n")
        for epoch, labels in enumerate(predicted_labels):
            file.write(f"Epoch {epoch + 1}:\n")
            file.write('\n'.join(str(label) for label in labels))
            file.write('\n\n')

def load_trained_data(input_file):
    with open(input_file, 'r') as file:
        lines = file.readlines()
        train_idx = lines.index("Train Accuracy:\n")
        val_idx = lines.index("Validation Accuracy:\n")
        labels_idx = lines.index("Predicted Labels:\n")

        for i in range(train_idx + 1, val_idx):
            line = lines[i].strip()
            if line:
                acc = float(line)
                train_acc.append(acc)

        for i in range(val_idx + 1, len(lines)):
            line = lines[i].strip()
            if line:
                acc = float(line)
                val_acc.append(acc)

        for i in range(labels_idx + 1, len(lines)):
            line = lines[i].strip()
            if line:
                labels = np.fromstring(line, dtype=float, sep=' ')
                predicted_labels.append(labels)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    train()
    #load_trained_data("output.txt")


    # Create the main window
    main_window = MainWindow(train_acc, val_acc)
    main_window.show()

    sys.exit(app.exec_())
